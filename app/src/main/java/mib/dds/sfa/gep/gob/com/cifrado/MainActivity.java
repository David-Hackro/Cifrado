package mib.dds.sfa.gep.gob.com.cifrado;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private TextView cifrado;
    private EditText palabra,key;
    private  Utils utils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        palabra = (EditText) findViewById(R.id.palabra);
        key = (EditText) findViewById(R.id.key);
        cifrado = (TextView) findViewById(R.id.cifrado);
        utils = new Utils();


    }

    public void Cifrado(View v)
    {
        String palabraCifrada = utils.Cifrado(palabra.getText().toString(),Integer.valueOf(key.getText().toString()));

        cifrado.setText(palabraCifrada);
    }

    public void DesCifrado(View v)
    {
        Toast.makeText(getApplicationContext(),utils.DesCifrado(cifrado.getText().toString(),Integer.valueOf(key.getText().toString())),Toast.LENGTH_LONG).show();

    }
}
