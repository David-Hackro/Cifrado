package mib.dds.sfa.gep.gob.com.cifrado;

/**
 * Created by hackro on 3/02/16.
 */
public  class Utils {



    public String Cifrado(String palabra,int  key)
    {
        String cadena= "";
        String CadenaFinal ="";

        if(Validacion(key)) {
        for (Character c : palabra.toCharArray()) {
            int ascii = (int) c;
            CadenaFinal+= (ascii *1)+",";
        }
            CadenaFinal = CadenaFinal.substring(0,CadenaFinal.length()-1);
        }
        else {
            palabra = new StringBuilder(palabra).reverse().toString(); //si el numero fue primo se invierte la palabra
            for (Character c : palabra.toCharArray()) {
                int ascii = (int) c;
                CadenaFinal += (ascii) + ",";
            }
        }
        return  CadenaFinal;
    }

    public  String DesCifrado(String palabraCodificada,int  key)
    {
        String cadenaFinal= "";

        String[] numbers = palabraCodificada.split(",");
        if(Validacion(key)){
        for (int i = 0; i < numbers.length; i++) {
            int temp = Integer.parseInt(numbers[i]);
            cadenaFinal+= Character.toString((char)temp);
        }
        }
        else {

            //palabraCodificada = new StringBuilder(palabraCodificada).reverse().toString();
            numbers = palabraCodificada.split(",");
            for (int i = 0; i < numbers.length; i++) {
                int temp = Integer.parseInt(numbers[i]);
                cadenaFinal+= Character.toString((char)temp);;
            }

            cadenaFinal = new StringBuilder(cadenaFinal).reverse().toString();
        }



        return  cadenaFinal;
    }


    public boolean Validacion(int n)
    {
        int a=0,i;
        for(i=1;i<(n+1);i++){
            if(n%i==0){
                a++;
            }
        }
        if(a!=2){
            return false;
        }else{
            return true;
        }
    }
}
